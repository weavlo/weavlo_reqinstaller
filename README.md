# weavlo_reqinstaller

This program will assist the user in installing ERPNext *prerequisite software*

### Types of ERPNext Users
When thinking about prerequisite software, it's helpful to consider your target audience:

1. **Test Drivers** - Someone wants to try ERPNext for the first time.  They typically take a fresh VPS, and try to install ERPNext quickly.
2. **SMBs and Entrepreneurs** - They are (usually) more comfortable allowing Installer Tools take control of prerequisites, setting up the database, etc.
3. **Medium-Enterprise Business** - In larger companies, an IT Department maintains *strict* control over software.
   1. Installation programs that try to *"sudo apt install everything"* will be rejected.
   2. System Administrators require detailed information on What was installed, Where, by Who, Which version, etc.
   3. They will often manually install SQL, Redis, Node, etc.

The *challenge* for [Weavlo](https://gitlab.com/weavlo) (and certainly the [Frappe](https://github.com/frappe/frappe) and [ERPNext](https://github.com/frappe/erpnext) maintainers) is to **support all 3 groups**.

Sometimes it "just needs to work."  Other times you need to be methodical when installing software, or distribute across many host servers.

## Requirements
- Program should *tell* the user what will be installed, and where.
- Program should make proper use of sudo, only when required.
- Program should output a Log File with results.
- Program should respect existing installed software.
- Program should ensure that pre-requisite software is configured to start on boot (Systemd, etc.)

## Regarding Containers
It seems reasonable to support Docker or Kubernetes containers.

1. This greatly simplfies installation.  We don't have to worry about target operating systems, and PreReqs of PreReqs.
2. It's arguably safer than modifying the host.
3. User can remove Dockerized pieces one at a time, and replace with enterprise versions, separate servers, etc.
